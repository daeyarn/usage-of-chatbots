#library used to parse the aiml file and get a bot's response
import aiml

#functions for converting text to speech and playing it
from gtts import gTTS
from playsound import playsound

#library for creating a graphical user interface
import tkinter as tk
import tkinter.scrolledtext as tkscrolled

import os
from time import sleep
from PIL import Image, ImageTk

FILES_DIRECTORY = "./"
FERBOT_AIML_FILE = FILES_DIRECTORY + "ferbot_version_4.aiml" #location of the aiml file containing bot's knowledge
USERNAME = "ME" #user's username
BOT_NAME = "FERBOT" #bot's username
LANGUAGE = "en" #language in which the bot talks, used for tts

BOT_TXT_COLOR = "green"
USER_TXT_COLOR = "orange"

#sets for mapping inputs
GREETINGS = ["GREETINGS", "HELLO", "HI", "HEY", "HOWDY"]
THANKS = ["THANKS", "THANK YOU", "TY", "TNX"]
POSITIVE = ["YEAH", "YES", "YUP", "SURE", "OKAY", "OK", "AFFIRMATIVE", "POSITIVE", "GOOD"]
NEGATIVE = ["NOPE", "NO", "NOT", "NAH", "NEGATIVE"]
GOODBYE = ["BYE", "GOODBYE", "BYE BYE", "BB", "SEE YOU", "SEE YA"]

class Bot:
    def __init__(self):

        #initialising the bot
        self.k = aiml.Kernel()

        #giving the bot the its knowledge(aiml file)
        self.k.learn( FERBOT_AIML_FILE )

        #creating the GUI
        self.window = tk.Tk()
        self.window.title("FERBOT")
        self.window.geometry("")
        
        self.window.configure(bg = "#5BC0DE")
        self.window.columnconfigure(0, weight=1)
        self.window.columnconfigure(1, weight=1)
        self.window.columnconfigure(2, weight=3)
        self.window.columnconfigure(3, weight=3)
        self.window.columnconfigure(4, weight=3)
        
        self.window.rowconfigure(0, weight=3)
        self.window.rowconfigure(1, weight=2)
        self.window.rowconfigure(2, weight=10)
        self.window.rowconfigure(3, weight=1)
        self.window.rowconfigure(4, weight=2)
        self.window.rowconfigure(5, weight=3)

        self.banner = tk.Frame(self.window)
        self.banner.columnconfigure(0, weight = 1)
        self.banner.columnconfigure(1, weight = 9)
        self.banner.config(bg = "#333333")

        self.ferbot_name = tk.Frame(self.window)
        self.ferbot_name.config(bg = "#00819c")

        #text area in which the conversation will be shown
        self.bot_output = tkscrolled.ScrolledText(self.window, bg = "#F5F5DC", width = 20, height = 5)
        self.bot_output.tag_config("bot", foreground = BOT_TXT_COLOR, background = "white")
        self.bot_output.tag_config("user", foreground = USER_TXT_COLOR, background = "white")
        self.bot_output["font"] = ("times new roman bold", 14)
        
        #text box which the user uses to talk to bot
        self.usr_input_box = tk.Frame(self.window)
        self.usr_input = tk.Text(self.window, height = 5)
        self.usr_input.tag_configure("center", justify='center')
        self.usr_input["font"] = ("times new roman", 14)
        
        self.usr_input.bind("<Return>", self.enter_key)

        self.bot_output.config(state="disabled")

        self.ferbottom = tk.Frame(self.window)
        self.ferbottom.config(bg = "#333333")
        
        self.bottom = tk.Frame(self.window)
        self.bottom.config(bg = "#00819c")
        
        self.banner.grid(column = 0, row = 0, columnspan = 5, rowspan = 1, sticky = "NESW")
        self.ferbot_name.grid(column = 0, row = 1, columnspan = 5, rowspan = 1, sticky = "NESW")

        self.bot_output.grid(column = 0, row = 2, columnspan = 1, rowspan = 1, sticky = "NESW")
        self.usr_input.grid(column = 0, row = 3, columnspan = 1, rowspan = 1, sticky = "NESW")

        self.bottom.grid(column = 0, row = 4, columnspan = 5, rowspan = 1, sticky = "NESW")
        self.ferbottom.grid(column = 0, row = 5, columnspan = 5, rowspan = 1, sticky = "NESW")

        #ferbot's appearance/avatar
        self.image = ImageTk.PhotoImage( file = "ferbot1.png" )
        self.blink_image = ImageTk.PhotoImage( file = "ferbot1_blink.png" )
        self.ferbot_img = tk.Label(self.window, image = self.image, bg = "#5BC0DE")
        self.ferbot_img.grid(column = 3, row = 2, rowspan = 2, sticky = "NESW")

        #frames used for animating when the bot is talking
        self.frames = [ImageTk.PhotoImage(file = "ferbot2.png"),
                       ImageTk.PhotoImage(file = "ferbot3.png"),
                       ImageTk.PhotoImage(file = "ferbot1.png")]

        self.after_id = self.window.after(1000, self.bot_animation_idle, 0)
        self.window.mainloop()

    #function for sending an input to the bot and getting an output on the screen and tts
    def enter_key(self, event):
        #on enter:
        #add new text to the label widget
        user_text = self.usr_input.get("1.0","end-1c")
        if user_text == "":
            return

        self.bot_output.config(state="normal")
        self.bot_output.insert("end", USERNAME + ": " + user_text.replace("\n","") + "\n", "user")
        self.bot_output.insert("end", "\n")
        self.bot_output.config(state="disabled")
        self.bot_output.see("end")
        
        #delete the text from the textbot
        self.usr_input.delete("1.0","end")
        self.usr_input.config(state = "disabled")

        self.window.update_idletasks()
        
        #get bot's response
        sleep(1)
        mapped_text = self.map_input(user_text)
        bot_text = self.k.respond(mapped_text)

        #add bot's response to the label widget
        self.bot_output.config(state="normal")
        self.bot_output.insert("end", BOT_NAME + ": " + bot_text + "\n", "bot")
        self.bot_output.insert("end", "\n")
        self.bot_output.config(state="disabled")
        self.bot_output.see("end")
        
        #speak bot's response
        tts = gTTS(text=bot_text, lang=LANGUAGE, slow=False)
        
        try:
            os.remove("bot_reply.mp3")
        except:
            ...
            
        tts.save("bot_reply.mp3")
        self.window.update_idletasks()
        
        playsound("bot_reply.mp3", block = False)

        self.window.after_cancel(self.after_id)

        #an estimation of how long the bot's sentence lasts
        #in order to sync it more accurately to the duration
        #of its mouth movement
        #the duration of talking can be precisely determined by
        #knowing the audio's length, in which case another library
        #for that purpose should be installed, but this estimation
        #is more than satisfactory
        length = len(bot_text.split(" "))
        avg_word_time = ( len(bot_text) - length - 1) / length
        avg_word_time = 5 if avg_word_time >= 5 else 3
        
        self.gif( avg_word_time, length )
        self.after_id = self.window.after(1300, self.bot_animation_idle, 0)

        self.usr_input.config(state = "normal")
        
        if bot_text == "Bye!":
            sleep(1)
            
            try:
                os.remove("bot_reply.mp3")
            except:
                ...
                
            self.window.destroy()
            exit(0)

    #function for making the bot blink while it is not talking
    def bot_animation_idle(self, event):
        self.ferbot_img.config(image = self.blink_image) #blink
        self.window.update_idletasks()

        #duration of the blink, not good to sleep on the GUI thread but
        #it happens so quickly so it is not noticeable
        sleep( 0.1 )
        
        self.ferbot_img.config(image = self.image) #open eyes
        self.window.update_idletasks()
        self.after_id = self.window.after(1500, self.bot_animation_idle, 0)


    #function for making the bot talk/move its mouth for a duration
    #depending on the length of a sentence
    def gif(self, avg_word_time, length):
        index = 0
        
        for i in range(0, avg_word_time*length):
            #delay making it look like the bot is moving its mouth and speaking
            #it's not good to sleep on the GUI thread but since the text input
            #is disabled anyway, it works
            sleep(0.1)

            frame = self.frames[index]
            index += 1
            if index == len(self.frames):
                index = 0
        
            self.ferbot_img.configure(image = frame)
            self.window.update_idletasks()
        
        self.ferbot_img.configure(image = self.image)

    #function used to map some user inputs
    #basically takes care of the patterns with wildcards
    #this should be done by the aiml kernel but I
    #couldn't get it to work
    def map_input(self, user_text):

        user_text = user_text.upper()
        sliced_text = user_text.replace(",", "").replace(".", "").replace(":", "").replace("!", "").replace("?", "")
        sliced_text = sliced_text.split(" ")

        if sliced_text[0] in GREETINGS:
            return "HI"

        if "ACRONYM" in sliced_text:
            return "ACRONYM"

        if "FER" in sliced_text and (("SHORT" in sliced_text and "FOR" in sliced_text) or "MEAN" in sliced_text or "MEANING" in sliced_text):
            return "ACRONYM"

        if "IS" in sliced_text and "FERBOT" in sliced_text:
            return "WHO IS FERBOT"

        if "PROGRAMMED" in sliced_text or "CREATED" in sliced_text or "CREATOR" in sliced_text or "MADE" in sliced_text:
            return "CREATED"

        if "FER" in sliced_text and "INFORMATION" in sliced_text:
            return "FER INFORMATION"

        if ("GO" in sliced_text or "ATTEND" in sliced_text) and "FER" in sliced_text:
            return "CAN I GO TO FER"

        if "PROGRAMME" in sliced_text or "PROGRAMMES" in sliced_text:
            return "STUDY PROGRAMME"

        if "HELP" in sliced_text or "ASSIST" in sliced_text or "ASSISTANCE" in sliced_text:
            return "HELP ME"

        if "WHY" in sliced_text and "FER" in sliced_text:
            return "WHY FER"

        if "PURPOSE" in sliced_text:
            return "PURPOSE"

        if "BACHELOR" in sliced_text or "BACHELORS" in sliced_text:
            return "BACHELOR"

        if "MASTER" in sliced_text or "MASTERS" in sliced_text:
            return "MASTER"

        if "ADDRESS" in sliced_text or "LOCATION" in sliced_text or ("WHERE" in sliced_text and "FER" in sliced_text):
            return "LOCATION"

        if user_text in THANKS:
            return "THANK YOU"

        if "WEBPAGE" in sliced_text or "WEB" in sliced_text:
            return "FER WEBPAGE"

        if "OLD" in sliced_text and "YOU" in sliced_text:
            return "OLD"

        if user_text in NEGATIVE:
            return "NO"

        if user_text in POSITIVE:
            return "YES"

        if "SEMESTER" in sliced_text or "SEMESTERS" in sliced_text:
            return "SEMESTER"

        if "ECTS" in sliced_text:
            return "ECTS"

        if "FER" in sliced_text and ("GOOD" in sliced_text or "BEST" in sliced_text):
            return "FER GOOD"

        if user_text in GOODBYE:
            return "BYE"
        
        return user_text

#main function
def main():
    #create bot object which consists of GUI and bot logic
    bot = Bot()

if __name__ == "__main__":
    main()
